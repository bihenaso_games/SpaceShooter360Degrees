//aliases
var Application = PIXI.Application,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite,
    Container = PIXI.Container,
    Text = PIXI.Text,
    Graphics = PIXI.Graphics;
//get screen sizes and ratio
var x = window.innerWidth,
    y = window.innerHeight,
    ratio = x / y,
    spritePath = "../images/";

//global declarations
var playerTexture,
    player,
    controlCircleTexture,
    controlCircle,
    controlCursorTexture,
    controlCursor;

//Constant value for sizing
var step = ratio < 1 ? x * 0.05: y * 0.05;

//create application
var app = new Application({
    width: x,
    height: y,
    antialias: true,
    transparent: false,
    resolution: window.devicePixelRatio
  }
);
//define app attribute and add it to document
app.renderer.backgroundColor = 0x0A0A08;
app.renderer.transparent = true;
app.renderer.view.style.display = "block";

document.body.appendChild(app.view);

function setup(){
  //define textures to make sprites
  playerTexture = new Texture.fromImage(spritePath + "playerShip1_red.png");
  controlCircleTexture = new Texture.fromImage(spritePath + "controlCircle.png");
  controlCursorTexture = new Texture.fromImage(spritePath + "cursor.png")

  //define sprites
  player = new Sprite(playerTexture);
  player.x = x * 0.5;
  player.y = y * 0.5;
  player.width = step;
  player.height = step;
  player.anchor.set(0.5);
  //Check first rotation with utility function findAngle
  player.rotation = findAngle(player.x, player.y, x * 0.5, y * 0.5);
  //Use to circularMove
  player.moveDirection = 0;
  player.moveVelocity = 0.01;
  player.ismove = false;
  app.stage.addChild(player);
  player.move = function(){
      //player move function
      //It call on pixi ticker
      //Calculate player location based on trigonmetric equations
      //It will provide circular motion 8 * step radius
      var newAngle = this.rotation + (this.moveDirection * this.moveVelocity);
      this.x = x * 0.5 + ((8 * step) * Math.cos(newAngle - Math.PI/2))
      this.y = y * 0.5 + ((8 * step) * Math.sin(newAngle - Math.PI/2));
      this.rotation = newAngle;
  }

  //Container contain out Joystcik elements
  controlContainer = new Container();
  app.stage.addChild(controlContainer);

  controlCircle = new Sprite(controlCircleTexture);
  controlCircle.x = 2 * step;
  controlCircle.y = 2 * step;
  controlCircle.width = 4 * step;
  controlCircle.height = 4 * step;
  controlCircle.alpha = 1;
  controlCircle.anchor.set(0.5);
  controlContainer.addChild(controlCircle);

  controlCursor = new Sprite(controlCursorTexture);
  controlCursor.x = controlCircle.x + (2 * step * Math.cos(player.rotation));
  controlCursor.y = controlCircle.y + (2 * step * Math.sin(player.rotation));
  controlCursor.width = 2 * step;
  controlCursor.height = 2 * step;
  controlCursor.anchor.set(0.5);
  controlCursor.alpha = 0.3;
  controlCursor.buttonMode = true;
  controlCursor.interactive = true;
  controlCursor.on('pointerdown', circularMoveBegin);
  controlCursor.on('pointermove', circularMove);
  controlCursor.on('pointerup', circularMoveEnd);
  controlCursor.on('pointerupoutside', circularMoveEnd);
  controlContainer.addChild(controlCursor);

  controlContainer.x = 3 * step;
  controlContainer.y = y - 3 * step;
  controlContainer.pivot.x = controlContainer.width * 0.5;
  controlContainer.pivot.y = controlContainer.height * 0.5;

  //pixi ticker
  //60 fps
  app.ticker.add(function(){
    player.move();
  })

}
function circularMoveBegin(event){
  //If Joystick is pushed trigger event
  //this event data include our Joystcik data
  //So, we can use this keyword in eventhandlers
  //After that we record start locations Of Joystcik
  //Our movement is circular, also we record first
  //angle
  this.data = event.data;
  this.flag = true;
  this.startX = this.data.getLocalPosition(this.parent).x;
  this.startY = this.data.getLocalPosition(this.parent).y;
  this.startAngle = findAngle(this.startX, this.startY, controlCircle.x, controlCircle.y);

}

function circularMove(event){
  //If event occur
  if(this.flag){
    //Find new angle
    var pointX = this.data.getLocalPosition(this.parent).x;
    var pointY = this.data.getLocalPosition(this.parent).y;
    var angle = findAngle(pointX, pointY, controlCircle.x, controlCircle.y);
    //If start angle less than new angle player move counter clockwise direction
    if(this.startAngle < angle){
      player.moveDirection = 1;
      controlCursor.x = controlCircle.x + (2 * step * Math.cos(angle));
      controlCursor.y = controlCircle.y + (2 * step * Math.sin(angle));
    }else if(this.startAngle > angle){
      //Else if start angle more than new angle player move clockwise direction
      player.moveDirection = -1;
      controlCursor.x = controlCircle.x + (2 * step * Math.cos(angle));
      controlCursor.y = controlCircle.y + (2 * step * Math.sin(angle));
    }else
      //If no change, player stayed same location
      player.moveDirection = 0;
  }
}

function circularMoveEnd(){
  this.data = null;
  this.flag = false;
  player.moveDirection = 0;
}

function findAngle(x,y,cX,cY){
  // atan2 is defined as the angle in the Euclidean plane, given in radians,
  //between the positive x axis and the ray to the point
  //It return radian value so return value is casted to degrees.
  var radian = Math.atan2(y-cY, x-cX);
  var angle = radian * (180/Math.PI);
  if(angle < 0.0){
    angle += 360.0;
  }
  return radian;
}

//In file load call setup function
window.onload = setup;
