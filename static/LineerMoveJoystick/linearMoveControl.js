//aliases
var Application = PIXI.Application,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite,
    Container = PIXI.Container,
    Text = PIXI.Text,
    Graphics = PIXI.Graphics;
//get screen sizes and ratio
var x = window.innerWidth,
    y = window.innerHeight,
    ratio = x / y,
    spritePath = "../images/";

//global declarations
var buttonOneTexture,
    buttonTwoTexture,
    buttonThreeTexture,
    buttonFourTexture,
    buttonFiveTexture,
    buttonSixTexture,
    playerTexture,
    lineerMoveJoystickTexture,
    buttonOne,
    buttonTwo,
    buttonThree,
    buttonFour,
    buttonFive,
    buttonSix,
    player,
    lineerMoveJoystick,
    distanceButtonsList = [];

//Constant value for sizing
var step = ratio < 1 ? x * 0.05: y * 0.05;

//create application
var app = new Application({
    width: x,
    height: y,
    antialias: true,
    transparent: false,
    resolution: window.devicePixelRatio
  }
);
//define app attribute and add it to document
app.renderer.backgroundColor = 0x0A0A08;
app.renderer.transparent = true;
app.renderer.view.style.display = "block";

document.body.appendChild(app.view);

function setup(){
  //define textures to make sprites
  playerTexture = new Texture.fromImage(spritePath + "playerShip1_red.png");
  buttonOneTexture = new Texture.fromImage(spritePath +"numeral1.png");
  buttonTwoTexture = new Texture.fromImage(spritePath +"numeral2.png");
  buttonThreeTexture = new Texture.fromImage(spritePath +"numeral3.png");
  buttonFourTexture = new Texture.fromImage(spritePath + "numeral4.png");
  buttonFiveTexture = new Texture.fromImage(spritePath + "numeral5.png");
  buttonSixTexture = new Texture.fromImage(spritePath + "numeral6.png");
  lineerMoveJoystickTexture = new Texture.fromImage(spritePath + "playerLife1_red.png");

  //define sprites
  player = new Sprite(playerTexture);
  player.x = x * 0.5;
  player.y = y * 0.5;
  player.width = step;
  player.height = step;
  player.anchor.set(0.5);
  player.moveRadius = 1;
  player.ismove = false;
  app.stage.addChild(player);
  player.move = function(){
      //player move function
      //It call on pixi ticker
      //Calculate player location based on trigonmetric equations
      this.x = x * 0.5 + (((this.moveRadius-1) * step) * Math.cos(this.rotation));
      this.y = y * 0.5 + (((this.moveRadius-1) * step) * Math.sin(this.rotation));
  }

  //Container contain out Joystcik elements
  lineerMoveJoystickContainer = new Container();
  app.stage.addChild(lineerMoveJoystickContainer);

  buttonOne = new Sprite(buttonOneTexture);
  buttonOne.x = step * 0.5;
  buttonOne.y = step * 6.75;
  buttonOne.width = step;
  buttonOne.height = step;
  buttonOne.anchor.set(0.5);
  buttonOne.alpha = 0.6;
  //these ids use as indicator
  buttonOne.id = 1;
  lineerMoveJoystickContainer.addChild(buttonOne);
  distanceButtonsList.push(buttonOne);

  buttonTwo = new Sprite(buttonTwoTexture);
  buttonTwo.x = step * 0.5;
  buttonTwo.y = step * 5.5;
  buttonTwo.width = step;
  buttonTwo.height = step;
  buttonTwo.anchor.set(0.5);
  buttonTwo.alpha = 0.3;
  buttonTwo.id = 2;
  lineerMoveJoystickContainer.addChild(buttonTwo);
  distanceButtonsList.push(buttonTwo);

  buttonThree = new Sprite(buttonThreeTexture);
  buttonThree.x = step * 0.5;
  buttonThree.y = step * 4.25;
  buttonThree.width = step;
  buttonThree.height = step;
  buttonThree.anchor.set(0.5);
  buttonThree.alpha = 0.3;
  buttonThree.id = 3;
  lineerMoveJoystickContainer.addChild(buttonThree);
  distanceButtonsList.push(buttonThree);

  buttonFour = new Sprite(buttonFourTexture);
  buttonFour.x = step * 0.5;
  buttonFour.y = step * 3;
  buttonFour.width = step;
  buttonFour.height = step;
  buttonFour.anchor.set(0.5);
  buttonFour.alpha = 0.3;
  buttonFour.id = 4;
  lineerMoveJoystickContainer.addChild(buttonFour);
  distanceButtonsList.push(buttonFour);

  buttonFive = new Sprite(buttonFiveTexture);
  buttonFive.x = step * 0.5;
  buttonFive.y = step * 1.75;
  buttonFive.width = step;
  buttonFive.height = step;
  buttonFive.anchor.set(0.5);
  buttonFive.alpha = 0.3;
  buttonFive.id = 5;
  lineerMoveJoystickContainer.addChild(buttonFive);
  distanceButtonsList.push(buttonFive);

  buttonSix = new Sprite(buttonSixTexture);
  buttonSix.x = step * 0.5;
  buttonSix.y = step * 0.5;
  buttonSix.width = step;
  buttonSix.height = step;
  buttonSix.anchor.set(0.5);
  buttonSix.alpha = 0.3;
  buttonSix.id = 6;
  lineerMoveJoystickContainer.addChild(buttonSix);
  distanceButtonsList.push(buttonSix);

  lineerMoveJoystick = new Sprite(lineerMoveJoystickTexture);
  lineerMoveJoystick.x = 1.75 * step;
  lineerMoveJoystick.y = step * 6.75;
  lineerMoveJoystick.width = step;
  lineerMoveJoystick.height = step;
  lineerMoveJoystick.anchor.set(0.5);
  lineerMoveJoystick.rotation = Math.PI * 1.5;
  lineerMoveJoystick.interactive = true;
  lineerMoveJoystick.buttonMode = true;
  lineerMoveJoystick.on('pointerdown', lineerMoveStart);
  lineerMoveJoystick.on('pointermove', lineerMove);
  lineerMoveJoystick.on('pointerupoutside', lineerMoveEnd);
  lineerMoveJoystick.on('pointerup', lineerMoveEnd);
  lineerMoveJoystickContainer.addChild(lineerMoveJoystick)

  //locate Joystcik container top of the page
  lineerMoveJoystickContainer.x =  x * 0.5;
  lineerMoveJoystickContainer.y = 1.25 * step;
  lineerMoveJoystickContainer.rotation = Math.PI / 2;
  lineerMoveJoystickContainer.pivot.x = lineerMoveJoystickContainer.width * 0.5;
  lineerMoveJoystickContainer.pivot.y = lineerMoveJoystickContainer.height * 0.5;

  //pixi ticker
  //60 fps
  app.ticker.add(function(){
    player.move();
  })

}

function lineerMoveStart(event){
  //If Joystick is pushed trigger event
  //this event data include our Joystcik data
  //So, we can use this keyword in eventhandlers
  //After that we record start locations Of Joystcik
  this.data = event.data;
  this.flag = true;
  this.startX = this.data.getLocalPosition(this.parent).x;
  this.startY = this.data.getLocalPosition(this.parent).y;
}
function lineerMove(event){
  //If event occur and do not end
  if(this.flag){
    //Get new location of Joystcik
    var pointX = this.data.getLocalPosition(this.parent).x;
    var pointY = this.data.getLocalPosition(this.parent).y;
    var diff = pointY - this.startY;
      //if Joystcik are pushed enough
      if(Math.abs(diff) > step * 0.75){
        //Check pushing direction and change player move radius
        if(diff < 0){
          if(player.moveRadius < 6)
            player.moveRadius++;
        }else if(diff > 0){
          if(player.moveRadius > 1)
            player.moveRadius--;
        }
        this.startX = pointX;
        this.startY = pointY;
        //Light up the number indicating the player's position
        distanceButtonsList.forEach(function(button){
          if(button.id == player.moveRadius){
            button.alpha = 0.6;
            lineerMoveJoystick.y = button.y;
          }else{
            button.alpha = 0.3;
          }
        }.bind(this));

      }
  }
}
function lineerMoveEnd(event){
  //If event end, reset data to next event
  this.data = null;
  this.flag = false;
}
//In file load call setup function
window.onload = setup;
